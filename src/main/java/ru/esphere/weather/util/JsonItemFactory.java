package ru.esphere.weather.util;

import org.springframework.stereotype.Component;
import ru.esphere.weather.dto.JsonItem;

@Component
public final class JsonItemFactory {

    public <T> JsonItem<T> successResult() {
        JsonItem<T> result = new JsonItem<>();
        result.setSuccess(true);
        result.setCode(200);
        return result;
    }

    public <T> JsonItem<T> failResult() {
        JsonItem<T> result = new JsonItem<>();
        result.setSuccess(false);
        result.setCode(500);
        return result;
    }

    public <T> JsonItem<T> successResult(T data) {
        JsonItem<T> result = new JsonItem<>();
        result.setSuccess(true);
        result.setData(data);
        result.setCode(200);
        return result;
    }

    public <T> JsonItem<T> failResult(int code, String message) {
        JsonItem<T> result = new JsonItem<>();
        result.setSuccess(false);
        result.setCode(code);
        result.setMessage(message);

        return result;
    }

}
