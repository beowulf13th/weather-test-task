package ru.esphere.weather.config;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@Import({
        MessageSourceConfig.class,
        WebMvcConfig.class,
        HttpConversationConfig.class,
})
@EnableAspectJAutoProxy(proxyTargetClass = true)
@ComponentScan(
        basePackages = "ru.esphere.weather",
        excludeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION, classes = Configuration.class)})
public class ApplicationConfig {
    /**
     * Spring boot like profile management
     */
    @Bean
    public static PropertyPlaceholderConfigurer propertiesFactoryBean() {
        PropertyPlaceholderConfigurer configurer = new PropertyPlaceholderConfigurer();
        String env = System.getProperty("env");
        if (env != null) {
            env = '-' + env;
        } else {
            env = "";
        }
        configurer.setLocation(new ClassPathResource("application" + env + ".properties"));
        return configurer;
    }
}