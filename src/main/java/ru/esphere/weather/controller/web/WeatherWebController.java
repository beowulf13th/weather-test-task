package ru.esphere.weather.controller.web;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.esphere.weather.service.api.WeatherService;

import java.util.Objects;

@Controller
@RequestMapping(WeatherWebController.ROOT_PATH)
@Log4j2
public class WeatherWebController {
    public static final String ROOT_PATH = "/weather";

    private final WeatherService weatherService;

    @Autowired
    public WeatherWebController(WeatherService weatherService) {
        this.weatherService = Objects.requireNonNull(weatherService);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String getWeatherPage() {
        return "weather";
    }
}