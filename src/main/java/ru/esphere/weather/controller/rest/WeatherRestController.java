package ru.esphere.weather.controller.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.esphere.weather.dto.JsonItem;
import ru.esphere.weather.dto.WeatherDto;
import ru.esphere.weather.exception.ApiException;
import ru.esphere.weather.service.api.WeatherService;
import ru.esphere.weather.util.JsonItemFactory;

import java.util.Collections;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping(WeatherRestController.ROOT_PATH)
public class WeatherRestController {
    public static final String ROOT_PATH = "/rest/weather";
    private final WeatherService weatherService;
    private final JsonItemFactory jsonItemFactory;

    @Autowired
    public WeatherRestController(WeatherService weatherService, JsonItemFactory jsonItemFactory) {
        this.weatherService = Objects.requireNonNull(weatherService);
        this.jsonItemFactory = Objects.requireNonNull(jsonItemFactory);
    }

    @RequestMapping(method = RequestMethod.GET)
    public JsonItem<String> requestCurrentWeather(@RequestParam("q") String query) {
        String result;
        try {
            result = weatherService.getCurrentWeather(Collections.singletonMap("q", query));
        } catch (ApiException e) {
            return jsonItemFactory.failResult(e.getStatusCode().value(), e.getMessage());
        }
        return jsonItemFactory.successResult(result);
    }

    @RequestMapping(value = "/result", method = RequestMethod.GET)
    public JsonItem<WeatherDto> getRequestedWeather(@RequestParam String uuid) {
        WeatherDto requestedWeather;
        try {
            requestedWeather = weatherService.getRequestedWeather(uuid);
        }
        catch (ApiException ae) {
            return jsonItemFactory.failResult(ae.getStatusCode().value(), ae.getMessage());
        }
        return jsonItemFactory.successResult(requestedWeather);
    }
}
