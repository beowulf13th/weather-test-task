package ru.esphere.weather.service.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import ru.esphere.weather.dto.ForecastDto;
import ru.esphere.weather.dto.QueryWeatherDto;
import ru.esphere.weather.dto.WeatherDto;
import ru.esphere.weather.exception.ApiException;
import ru.esphere.weather.service.api.WeatherService;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Log4j2
public class WeatherServiceImpl implements WeatherService {

    private final RestTemplate restTemplate;
    private final String apiUrl;
    private final String apiKey;

    private final LinkedList<QueryWeatherDto> queries = new LinkedList<>();
    private volatile ConcurrentHashMap<String, WeatherDto> results = new ConcurrentHashMap<>();
    private volatile boolean stopped;

    private Thread queryThread = new QueryHandler();

    private WeatherDto M = new WeatherDto();

    @Autowired
    public WeatherServiceImpl(RestTemplate restTemplate,
                              @Value("${weather.api.url}") String apiUrl,
                              @Value("${weather.api.key}") String apiKey) {
        this.restTemplate = Objects.requireNonNull(restTemplate);
        this.apiKey = apiKey;
        if (StringUtils.isEmpty(apiUrl)) {
            throw new NullPointerException("weather.api.url must be set in configuration file");
        }
        this.apiUrl = apiUrl;
    }

    @PostConstruct
    public void init() {
        log.debug("Run query handler");
        queryThread.start();
    }

    @PreDestroy
    public void destruct() {
        stopped = true;
    }

    @Override
    public String getCurrentWeather(Map<String, String> params) {
        log.debug("Add event");
        String uuid = UUID.randomUUID().toString();
        synchronized (queries) {
            queries.add(new QueryWeatherDto(uuid, params));
            queries.notifyAll();
        }
        results.put(uuid, M);
        return uuid;
    }

    @Override
    public Optional<ForecastDto> getForecast(Map<String, String> params) {
        buildRequestUrl("/forecast.json", params);
        return Optional.empty();
    }

    @Override
    public WeatherDto getRequestedWeather(String uuid) {
        if (!results.containsKey(uuid)) {
            log.warn("UUID={} not found in queue", uuid);
            throw new ApiException(HttpStatus.NOT_FOUND, "Weather not queried");
        }
        WeatherDto weatherDto = results.get(uuid);
        if (Objects.equals(weatherDto, M)) {
            log.info("Weather with UUID={} still in queue", uuid);
            throw new ApiException(HttpStatus.NO_CONTENT, "Weather already in queue");
        }
        results.remove(uuid);
        return weatherDto;
    }

    private void processWeather(QueryWeatherDto dto) {
        log.debug("ProcessWeather for query {}", dto.getQuery().get("q"));
        ResponseEntity<WeatherDto> currentWeather;
        try {
            currentWeather = restTemplate.getForEntity(buildRequestUrl("/current.json", dto.getQuery()), WeatherDto.class);
        } catch (HttpClientErrorException hcee) {
            log.warn(hcee.getMessage());
            WeatherDto value = new WeatherDto();
            value.setErrorMessage(hcee.getMessage());
            value.setStatus(400);
            results.put(dto.getUuid(), value);
            return;
        }
        if (!currentWeather.getStatusCode().is2xxSuccessful()) {
            log.warn("Api response is {}", currentWeather.getStatusCode());
            WeatherDto value = new WeatherDto();
            value.setErrorMessage("Api response error");
            value.setStatus(400);
            results.put(dto.getUuid(), value);
            return;
        }
        log.debug("Store result to {}" + dto.getUuid());
        results.put(dto.getUuid(), currentWeather.getBody());

        try {
            Thread.sleep(500); // TODO stub for debug
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String buildRequestUrl(String resource, Map<String, String> params) {
        log.debug("Build url to {}", resource);
        StringJoiner joiner = new StringJoiner("&");

        params.forEach((k, v) -> joiner.add(k + '=' + v));
        String url = apiUrl + resource + '?' + joiner.toString() + "&key=" + apiKey;
        log.trace(url);
        return url;
    }

    private class QueryHandler extends Thread {
        @Override
        public void run() {
            log.debug("Start QueryHandler");
            while (!stopped) {
                QueryWeatherDto query = null;
                synchronized (queries) {
                    while (!stopped) {
                        query = queries.poll();
                        log.debug("Process {}", query);
                        if (query == null) {
                            try {
                                queries.notifyAll();
                                queries.wait();
                                continue;
                            } catch (InterruptedException e) {
                                log.error(e.getMessage());
                            }
                        }
                        break;
                    }
                }

                if (query != null) {
                    processWeather(query);
                }
            }
            log.debug("Leave QueryHandler");
        }
    }
}