package ru.esphere.weather.service.api;

import ru.esphere.weather.dto.ForecastDto;
import ru.esphere.weather.dto.WeatherDto;

import java.util.Map;
import java.util.Optional;

public interface WeatherService {
    String getCurrentWeather(Map<String, String> params);

    Optional<ForecastDto> getForecast(Map<String, String> params);

    WeatherDto getRequestedWeather(String uuid);
}