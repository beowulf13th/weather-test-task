package ru.esphere.weather.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class JsonItem<T> implements Serializable {
    private static final long serialVersionUID = 7829843294020683215L;

    @JsonProperty(required = true)
    private boolean success;
    private T data;
    private String message;
    private int code;
}