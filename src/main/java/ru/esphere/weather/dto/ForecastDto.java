package ru.esphere.weather.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class ForecastDto implements Serializable {
    private static final long serialVersionUID = 8539653268553245987L;

    @JsonProperty("forecastday")
    private List<ForecastDayDto> forecastDays;
}