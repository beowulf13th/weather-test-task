package ru.esphere.weather.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class LocationDto implements Serializable {
    private static final long serialVersionUID = -4608843022584581684L;

    private String name;

    private String region;

    private String country;

    @JsonProperty("tz_id")
    private String timeZoneId;

    private String localtime;

    private double lat;

    private double lon;

    @JsonProperty("localtime_epoch")
    private int localtimeEpoch;
}