package ru.esphere.weather.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class ConditionDto implements Serializable {
    private static final long serialVersionUID = 4863956179287544666L;
    private String text;
	private String icon;
	private int code;
}
