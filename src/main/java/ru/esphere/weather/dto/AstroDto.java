package ru.esphere.weather.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class AstroDto implements Serializable {
	private static final long serialVersionUID = -4826254723302683419L;

    @JsonProperty("sunrise")
	private String sunrise;
	
	@JsonProperty("sunset")
	private String sunset;
	
	@JsonProperty("moonrise")
	private String moonrise;
	
	@JsonProperty("moonset")
	private String moonset;
}