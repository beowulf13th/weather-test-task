package ru.esphere.weather.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class WeatherDto implements Serializable {
    private static final long serialVersionUID = -422665403106344796L;
    private LocationDto location;
    private CurrentDto current;
    private ForecastDto forecast;
    private String errorMessage;
    private int status;
}