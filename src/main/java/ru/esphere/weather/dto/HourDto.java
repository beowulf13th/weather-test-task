package ru.esphere.weather.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class HourDto implements Serializable {
    private static final long serialVersionUID = 8711095322842130148L;

    @JsonProperty("time_epoch")
    private int timeEpoch;

    @JsonProperty("wind_degree")
    private int windDegree;

    private int humidity;

    private int cloud;

    @JsonProperty("will_it_rain")
    private int willItRain;

    @JsonProperty("will_it_snow")
    private int willItSnow;

    private String time;

    @JsonProperty("wind_dir")
    private String windDirection;

    @JsonProperty("temp_c")
    private double tempCelsius;

    @JsonProperty("temp_f")
    private double tempFahrenheit;

    @JsonProperty("wind_mph")
    private double windMph;

    @JsonProperty("wind_kph")
    private double windKph;

    @JsonProperty("pressure_mb")
    private double pressureMb;

    @JsonProperty("pressure_in")
    private double pressureIn;

    @JsonProperty("precip_mm")
    private double precip_mm;

    @JsonProperty("precip_in")
    private double precip_in;

    @JsonProperty("feelslike_c")
    private double feelsLikeCelsius;

    @JsonProperty("feelslike_f")
    private double feelsLikeFahrenheit;

    @JsonProperty("windchill_c")
    private double windChillCelsius;

    @JsonProperty("windchill_f")
    private double windChillFahrenheit;

    @JsonProperty("heatindex_c")
    private double heatIndexCelsius;

    @JsonProperty("heatindex_f")
    private double heatIndexFahrenheit;

    @JsonProperty("dewpoint_c")
    private double dewPointCelsius;

    @JsonProperty("dewpoint_f")
    private double dewPointFahrenheit;

    private ConditionDto condition;
}