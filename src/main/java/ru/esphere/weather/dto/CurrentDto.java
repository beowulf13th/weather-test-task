package ru.esphere.weather.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class CurrentDto implements Serializable {
    private static final long serialVersionUID = 5572680018676947407L;

    @JsonProperty("last_updated_epoch")
    private int lastUpdatedEpoch;

    @JsonProperty("wind_degree")
    private int windDegree;

    private int humidity;

    private int cloud;

    @JsonProperty("last_updated")
    private String lastUpdated;

    @JsonProperty("wind_dir")
    private String windDir;

    @JsonProperty("temp_c")
    private double tempCelsius;

    @JsonProperty("temp_f")
    private double tempFahrenheit;

    @JsonProperty("wind_mph")
    private double windMph;

    @JsonProperty("wind_kph")
    private double windKph;

    @JsonProperty("pressure_mb")
    private double pressureMb;

    @JsonProperty("pressure_in")
    private double pressureInch;

    @JsonProperty("precip_mm")
    private double precipitateMm;

    @JsonProperty("precip_in")
    private double precipitateInch;

    @JsonProperty("feelslike_c")
    private double feelsLikeCelsius;

    @JsonProperty("feelslike_f")
    private double feelsLikeFahrenheit;

    @JsonProperty("is_day")
    private int isDay;
}