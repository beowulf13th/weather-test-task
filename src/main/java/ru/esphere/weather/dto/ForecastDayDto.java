package ru.esphere.weather.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class ForecastDayDto implements Serializable {
    private static final long serialVersionUID = 4231732290839076663L;

    @JsonProperty("date")
	private String date;

	@JsonProperty("date_epoch")
	private int dateEpoch;

	@JsonProperty("day")
	private DayDto day;

	@JsonProperty("astro")
	private AstroDto astro;

	@JsonProperty("hour")
	private List<HourDto> hour;
}