package ru.esphere.weather.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class DayDto implements Serializable {
	private static final long serialVersionUID = -5460830826789194378L;
	
	@JsonProperty("maxtemp_c")
	private double maxTempCelsius;
	
	@JsonProperty("maxtemp_f")
	private double maxTempFahrenheit;
	
	@JsonProperty("mintemp_c")
	private double minTempCelsius;
	
	@JsonProperty("mintemp_f")
	private double minTempFahrenheit;
	
	@JsonProperty("avgtemp_c")
	private double averageTempCelsius;
	
	@JsonProperty("avgtemp_f")
	private double averageTempFahrenheit;
	
	@JsonProperty("maxwind_mph")
	private double maxWindMph;
	
	@JsonProperty("maxwind_kph")
	private double maxWindKph;
	
	@JsonProperty("totalprecip_mm")
	private double totalPrecipitateMm;
	
	@JsonProperty("totalprecip_in")
	private double totalPrecipitateInch;

	private ConditionDto condition;
}