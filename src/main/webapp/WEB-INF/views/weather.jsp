<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title><spring:message code="weather.title"/></title>
    <link type="text/css" href="<c:url value="/static/css/bootstrap.min.css"/>" rel="stylesheet"/>
    <script src="<c:url value="/static/js/jquery.js"/>" type="text/javascript"></script>
    <script src="<c:url value="/static/js/popper.js"/>" type="text/javascript"></script>
    <script src="<c:url value="/static/js/bootstrap.bundle.js"/>" type="text/javascript"></script>
    <script type="text/javascript">
        let restBase = "<c:url value="/rest"/>";
        let IN_QUEUE = "<spring:message code="weather.request.in.queue"/>";
        let ERROR_500 = "<spring:message code="weather.error500"/>";
        let CITY_NOT_FOUND = "<spring:message code="weather.city.not.found"/>";

        let TEMPERATURE_TEXT = "<spring:message code="weather.temperature"/>";
        let HUMIDITY_TEXT = "<spring:message code="weather.humidity"/>";
        let WIND_TEXT = "<spring:message code="weather.wind"/>";
    </script>
</head>
<body>
<section>
    <div>
        <div class="container">
            <div class="row">
                <div class="col-sm-10 offset-sm-1 text-center">
                    <h1 class="display-4"><spring:message code="weather.form.header"/></h1>
                    <div class="mb-1">
                        <input type="text" class="js-weather-query form-control" placeholder="<spring:message code="weather.form.hint"/>"/>
                    </div>
                    <div class="input-group align-content-center">
                        <%--<div class="input-group-prepend">
                            <button type="button" class="btn btn-primary js-find-forecast" data-days="1">forecast</button>
                        </div>
                        <div class="input-group-prepend">
                            <button type="button" class="btn btn-success js-find-forecast" data-days="3">forecast 3 days</button>
                        </div>
                        <div class="input-group-prepend">
                            <button type="button" class="btn btn-primary js-find-forecast" data-days="5">forecast 5 days</button>
                        </div>--%>
                        <div <%--class="input-group-append"--%>>
                            <button type="button" class="btn btn-danger js-find-weather"><spring:message code="weather.form.search"/></button>
                        </div>
                    </div>
                    <span>↓</span>
                    <span class="js-loader" style="display: none;"><spring:message code="weather.loading"/></span>
                    <div class="js-weather-data">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="<c:url value="/static/js/app.js"/>"></script>
</body>
</html>