let $loader = $(".js-loader");
let $weatherData = $(".js-weather-data");

/** not used **/
function findForecast(event) {
    let days = $(event.target).data("days");
    $.ajax({
        url: restBase + "/forecast"
    });
}

var uuid = null;

function showWeather(data) {
    if (data.success === false) {
        if (data.code === 204) {
            $loader.html(IN_QUEUE);
            setTimeout(function () {
                requestWeather(uuid);
            }, 1000);
        }
    }
    else {
        if (data.data.status === 0) {
            drawResult(data.data);
        }
        else {
            showError(data.data.errorMessage, data.data.status)
        }
    }
}

function drawResult(weather) {
    if (weather.code !== 200) {
        $weatherData.html(weather.location.country + ", " + weather.location.region + ", " + weather.location.name
            + "<br/>" + TEMPERATURE_TEXT + " " + weather.current.temp_c + "C"
            + "<br/>" + HUMIDITY_TEXT + " " + weather.current.humidity + "%"
            + "<br/>" + WIND_TEXT + " " + weather.current.wind_dir + " " + weather.current.wind_kph + "km/h");
        $loader.hide();
        $weatherData.show();
        uuid = null;
    }
}

function showError(error, code) {
    if (code === 500) {
        $weatherData.html(ERROR_500);
    }
    else if (code === 400) {
        $weatherData.html(CITY_NOT_FOUND);
    }
    else {
        $weatherData.html(error);
    }
    $loader.hide();
    $weatherData.show();
    uuid = null;
}

function findWeather() {
    $weatherData.hide();
    $loader.show();
    $.ajax({
        url: restBase + "/weather?q=" + $(".js-weather-query").val(),
        method: 'get',
    }).done(
        function (data) {
            if (uuid === null) {
                uuid = data.data;
            }
            requestWeather(uuid);
        }
    ).fail(showError)
}

function requestWeather(uuid) {
    $.ajax({
        url: restBase + "/weather/result?uuid=" + uuid,
        method: 'get',
    }).done(showWeather).fail(showError)
}

$(document).ready(function () {
    let $forecastButton = $(".js-find-forecast");
    let $weatherButton = $(".js-find-weather");

    $forecastButton.on('click', findForecast);
    $weatherButton.on('click', findWeather);
});